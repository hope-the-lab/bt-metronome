var readline = require('readline');
var goline = {
	ini: function(){
		this.rl.setPrompt(goline.prefix, goline.prefix.length);
	},
	go: function (msg, funl) {
		this.rl.question(msg,funl);
	},
	rl :readline.createInterface(process.stdin, process.stdout),
	prefix : 'BTcontroll > ',
	helper_ini: function (BTctr) {
		var base = this;
		base.rl.on('line', function (line) {
			if (line == 'helper'||line == 'h') {
				console.log('------------------------ \n');
				console.log('open helper system:');
				
				var option = (function(opt){
					var str = '';
					for ( var i in opt ){
						str += i +". "+ opt[i].name +" \n"
					}
					return str;
				})(base.helper_options);
				base.rl.question(option,function(input){
                    var hc = base.helper_options;
					switch(input){
                        case '0' :
                            hc[0].fuc(BTctr.noble);
                            break;
                        case '1' :
                            hc[1].fuc(BTctr.noble);
                            break;
                        case '2' :
                            hc[2].fuc(BTctr,base);
                            break;
						case '3' :
                            hc[3].fuc(BTctr);
                            break;
						case '4' :
                            hc[4].fuc(BTctr);
                            break;
						case '5' :
                            hc[5].fuc(BTctr,base);
                            break;
						case '6' :
                            hc[6].fuc(BTctr);
                            break;
                        default :
                            console.log("sorry I can't help");
                            break;
                    }
				});
			}
			if(line == 'pause'||line=='p'){
				for(var i in BTctr.data.connect_list){
					var btper = BTctr.data.connect_list[i]
					btper.disconnect();
					btper.on('disconnect',function(){
						console.log(this.advertisement.localName+' is disconnect')
					});
				}
				
			}
			if(line == 'auto'||line=='a'){
				base.helper_options["6"].fuc(BTctr);
			}
			/*if(line == 'look'||line=='l'){
				console.log('try overlookPeripheral')
				base.rl.question('key num :', function (input) {
					if (parseInt(input) >= BTctr.data.per_list.length) {
						console.log('device dont exist');
					}
					else {
						BTctr.overlookPeripheral(BTctr.data.per_list[input]);
					}
				});
			}*/
		});
	},
	helper_options: {
		'0': {
			name: 'stopScaning'
			, fuc: function (noble) {
				noble.stopScanning();
			}
		},
		'1': {
			name: 'startScaning'
			, fuc: function (noble) {
				noble.startScanning();
			}
		},
		'2': {
			name: 'selectDeivce'
			, fuc: function (BTctr, base) {
				base.rl.question('key num :', function (input) {
					if (parseInt(input) >= BTctr.data.per_list.length) {
						console.log('device dont exist');
					}
					else {
						BTctr.selectDevice(input);
					}
				});
			}
		},
		'3': {
			name: 'discoverDevice'
			, fuc: function (BTctr) {
				BTctr.discoverDevice();
			}
		},
		'4': {
			name: 'listSelectDevice',
			fuc: function (BTctr) {
				BTctr.listSelectDevice();
			}
		},
		'5':{
			name:'choiceScript',
			fuc:function(BTctr,base){
				console.log('---------script list-----------');
				var p_list = (function(){
					var string = '';
					for( var i in BTctr.script){
						string += "\n" +i+'. '+BTctr.script[i].name;
					}
					return string;
					})();
				console.log(p_list);
				base.rl.question('key num',function(input){
					if(parseInt(input)>=Object.keys(BTctr.script).length){
						console.log('script desnt exist.');
					}else{
						BTctr.goScript = BTctr.script[input].func;
						console.log(BTctr.script[input].name+' is selected');
					}
					
				});
			}
		},
		'6': {
				name: 'startConnect',
				fuc: function (BTctr) {
					BTctr.startConnect();
				}
			}
	}
}
goline.ini();
module.exports = goline;