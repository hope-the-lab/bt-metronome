var goline = require('./goline.js'),
    noble = require('noble'),
    fs = require("fs"),
	bt_config = JSON.parse(fs.readFileSync("app_config.json"));
console.log('app start');
noble.on('stateChange', function (state) {
	console.log('on -> stateChange: ' + state);
	if (state === 'poweredOn') {
		noble.startScanning();
	}
	else {
		noble.stopScanning();
	}
});
noble.on('scanStart', function () {
	Btcontroll.ini();
	console.log('on -> scanStart');
});
noble.on('scanStop', function () {
	console.log('on -> scanStop');
});
noble.on('discover', function (peripheral) {
	
	Btcontroll.add_per_list(peripheral);
	Btcontroll.defultListener(peripheral);
	if(peripheral.advertisement.localName=="BT05"){
		Btcontroll.data.select_list.push(peripheral);
		console.log('----------------------------');
		console.log(peripheral.advertisement.localName+' is automatically selected');
		console.log('----------------------------');
	}
});

var Btcontroll = {
	ini : function(){
		this.data.per_list = [];
		this.data.select_list = [];
		this.data.connect_list = [];
	},
	data: {
		per_list: [],
		select_list: [],
		connect_list :[]
	}
}
Btcontroll.config=bt_config;
Btcontroll.noble=noble;
Btcontroll.helper=goline.helper_ini(Btcontroll);
Btcontroll.add_per_list=function(per){
	this.data.per_list.push(per);
	var list =(function(p){
		var temp_list = []
		for( var i in p ){
			temp_list.push("\n[<"+i+">."+p[i].advertisement.localName+":"+p[i].address+"]");
		}
		return temp_list;
	})(this.data.per_list);
	console.log('Discover Device :'+list);
};
Btcontroll.selectDevice=function(lineSelect){
    this.data.select_list.push(this.data.per_list[lineSelect]);
    console.log(this.data.per_list[lineSelect].advertisement.localName+' is selected');
};
Btcontroll.discoverDevice = function () {
	var list = (function (p) {
		var temp_list = []
		for (var i in p) {
			temp_list.push("\n[<" + i + ">." + p[i].advertisement.localName + ":" + p[i].address + "]");
		}
		return temp_list;
	})(this.data.per_list);
	console.log('Discover Device :'+list);
};
Btcontroll.listSelectDevice =function(){
	var list = (function (p) {
		var temp_list = []
		for (var i in p) {
			temp_list.push("\n[<" + i + ">." + p[i].advertisement.localName + ":" + p[i].address + "]");
		}
		return temp_list;
	})(this.data.select_list);
	console.log('selected Device :'+list);
};
Btcontroll.startConnect = function(){
	this.noble.stopScanning();
	var per_list = this.data.select_list;
	var base = this;
	for ( var i in per_list){
		per_list[i].connect();
	}
};
Btcontroll.listConnectDevice = function(per){
	this.data.connect_list.push(per);
};
Btcontroll.defultListener=function(peripheral){
	var base = this;
	peripheral.on('connect', function () {
		console.log('on -> '+peripheral.advertisement.localName+' connect');
		base.listConnectDevice(peripheral);
		this.discoverServices();
	});
	peripheral.on('disconnect', function () {
		console.log('on -> disconnect');
	});
	peripheral.on('servicesDiscover', function (services) {
		console.log('on -> '+peripheral.advertisement.localName+' services discovered: ');
		var serviceIndex = base.config.serviceIndex;
		services[serviceIndex].on('characteristicsDiscover', function (characteristics) {
			base.goScript(characteristics,peripheral);
		});
		services[serviceIndex].discoverCharacteristics();
	});
};
Btcontroll.script={
	'0':{
		name:'defult Write & Read',
		func:function(characteristics,per){
			var buff = new Buffer(Btcontroll.config.defultRWString);
			characteristics[0].write(buff);
			characteristics[0].read();
			characteristics[0].on('read',function(data){
				console.log('\n');
				console.log('read : '+data);
				console.log(data);
				console.log('\n');
				per.disconnect();
			});
		}
	},
	'1':{
		name:'read all properties',
		func:function(characteristics,per){
			for(var i in characteristics){
				var buff = new Buffer('o');
				characteristics[i].write(buff);
				characteristics[i].read();
				console.log(i+' has been read');
				(function(index){
					characteristics[index].on('read',function(data){
					console.log('['+index+'] '+ data);
					console.log(data);
					console.log('\n');
				});
				})(i);
			}
		}
 },
    '2':{
        name:'key any string',
        func:function(characteristics,per){
            goline.rl.question('key the string : ',function(input){
                var buff = new Buffer(input);
                characteristics[0].write(buff);
                characteristics[0].read();
                characteristics[0].on('read',function(data){
				console.log('\n');
				console.log('read : '+data);
				console.log(data);
				console.log('\n');
				per.disconnect();
			});
            });
        }
    },
    '3':{
            name:'always read',
            func:function(characteristics,per){
                characteristics[0].read();
                characteristics[0].on('read',function(data){
                    console.log('read : '+ data);
                    console.log(data);
                    this.read();
                });
            }
        },
    '4':{
        name:'pin forever',
        func:function(characteristics,per){
			console.log('write : 01234567')
            var data ={
				average:[],
				steper:0
			}
            function pin(){
                characteristics[0].write(new Buffer('01234567'));
                characteristics[0].read();
            }
            //characteristics[0].write(data.buff);
            var looper = setInterval(function(){
                pin();
				data.time1a=new Date().getTime();
            },200);
            
            characteristics[0].on('read',function(tag){
				data.time1b=new Date().getTime();
				data.average[data.steper]=(data.time1b - data.time1a);
				var result = (function(){
					var a = 0;
					for(var i in data.average){
						a+=data.average[i]/data.average.length;
					}
					return a;
				})();
				if (data.steper==0){
					console.log('average latency : '+ parseInt(result));
				}
                data.steper=(data.steper>9)?0:data.steper+1;
            });
			per.on('disconnect',function(){
				clearInterval(looper);
			});
        }
    }
    
};
Btcontroll.goScript = Btcontroll.script[Btcontroll.config.defultScript].func;
/*
Btcontroll.overlookPeripheral=function(per){
    Btcontroll.data.connect_list.push(per);
	console.log('-----------------START print Peripheral---------------------');
	console.log(per.toString());
	console.log('------------list service');
	per.connect();
	per.on('connect', function () {this.discoverServices();});
	per.on('servicesDiscover',function(services){
		for ( var i in services){
			(function(index,service){
				service.on('characteristicsDiscover', function (characteristics) {
					console.log('----'+index+'--------');
				    console.log(service.toString());
					console.log('characteristics : \n'+characteristics);
				});
			})(i,services[i])
			services[i].discoverCharacteristics();
		}
	});
}
*/