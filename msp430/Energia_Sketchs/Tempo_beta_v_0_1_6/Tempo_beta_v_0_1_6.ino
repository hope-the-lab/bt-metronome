char Data = 0;                    // for got serial data
char char_buffer[8];              // for got BPM time in char
int  BPM_num[4] = {0,0,0,0};      // for got BPM time in int
int  BPM = 0;                     // Real BPM number
char Sync = '0';                  // Real Sync contorl
int Syncdelay = 1000;             // Real Sync time
int check_Sync = 0;

String longDatas = "\0";  // for saving char "Data"
String Data_buffer = "\0";// for saving String "longDatas"
String AT_Str0 = "at";
String AT_Str1 = "AT";

void setup() 
{
  
    Serial.begin(9600);    // opens serial port, sets data rate to 9600 bps
  
    pinMode(2, OUTPUT);   // set for Green LED
    pinMode(7, OUTPUT);   // set for DC Mortor
  
    Serial.println("[Ver. beta 1.6]");        // Version beta 1.6
    Serial.println("Hello!");
    Serial.println("*** Welcome to MSP430's Beats project. ***");
    Serial.println();
    Serial.println("[Notice] wait for S2 & BPM to test.");
    Serial.println("    Have fun :)");   // Ready to command
}

void loop()
{
           
  //  do
    if(Sync == '1')
    {
         if(BPM > 0)  //Active BPM !
            { 
         //   BPM_beats(BPM);  // Ex : BPM:60
                digitalWrite(7, HIGH);           // Mortor  ON
                digitalWrite(2, HIGH);           // Green ON
                delay(75);
          
                digitalWrite(7, LOW);            // Mortor   OFF 
                digitalWrite(2, LOW);            // Green OFF       
                delay(BPM - 75);      
            }  
          else       //Sync but no BPM output.
            {
                digitalWrite(2, HIGH);
                delay(Syncdelay); 
                digitalWrite(2, LOW);   
                delay(Syncdelay); 
            }
    
    }
    //STOP Sync & BPM
    if(check_Sync == 0)   
     {delay(250);}
         digitalWrite(2, HIGH);  // Green LED Hold
         digitalWrite(7, LOW);   // Mortor Stop
         
       
       //noInterrupts();
interrupts();             
    if(Serial.available() > 0)            //Keyin to active
         {              
             longDatas = "\0";           // clear String buffer !
             while(Serial.available() > 0)
               {
                    Data = Serial.read();  // Got what you say                      
                    longDatas = String(longDatas + Data); // Save a char I got

//                    Serial.print(">>");
//                    Serial.println(Data);       //check
               }
              longDatas = String(longDatas + ' ');  // Add a null char
              Data_buffer = longDatas;               // Save Datas to buffer
              Serial.println(longDatas);              // String echo!        
              
             if (Data_buffer == "00000000 ")  
                {
                    Serial.println("BPM normal Stop.");
                    BPM = 0;
               //     Sync = '1';    // Stop BPM & Back to Sync mode.
                    Syncdelay = 1000;
                }  
             else if (Data_buffer.substring(0,2)=="S2")
                {    
                    Sync = '1';
                    Syncdelay = Syncdelay * 2; 
                    check_Sync = 1;   
                } // Go to Sync mode.           
             else if (Data_buffer.substring(0,1)=="S")  //show Delay time & BPM (ms)
                {
                     getBPM();
                     Serial.println("Get datas :");
                     Serial.println(BPM);
                }            
             else
                 {
//                   Serial.println("No command.");
                     Serial.println(Data_buffer);    // String echo! 
                 }
              
           }
/*           
    if()
           {
           
           } 
           */
           
}

void BPM_beats(int BPM)
{        
          digitalWrite(7, HIGH);           // Mortor  ON
          digitalWrite(2, HIGH);           // Green ON
          delay(75);
          
          digitalWrite(7, LOW);            // Mortor   OFF 
          digitalWrite(2, LOW);            // Green OFF       
          delay(BPM - 75);      
}

void getBPM()
{
    Data_buffer.toCharArray(char_buffer, 6);
//    int count;
    Serial.println(char_buffer);
    
    BPM_num[0] = char_buffer[1] - 48;
    BPM_num[1] = char_buffer[2] - 48;
    BPM_num[2] = char_buffer[3] - 48;
    BPM_num[3] = char_buffer[4] - 48;
  
   BPM =  BPM_num[0] * 1000 + BPM_num[1] * 100 + BPM_num[2] * 10 + BPM_num[3];
   //delay_time =  delay_num[0] * 100 + delay_num[1] * 10 + delay_num[2];
}
