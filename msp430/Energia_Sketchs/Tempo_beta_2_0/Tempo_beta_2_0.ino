char Data = 0;            // for got serial data
char BPM_buffer[8];       // for got BPM numbers in char
int  BPM_num[3] = {0,0,0};// for got BPM numbers in int
int  BPM=0;               // Real BPM number

String longDatas = "\0";  // for saving char "Data"
String Data_buffer = "\0";// for saving String "longDatas"
String AT_Str0 = "at";
String AT_Str1 = "AT";

int time = 0;

//volatile int flag0 = LOW;
//volatile int flag1 = LOW;

void setup() 
{
  
    Serial.begin(9600);    // opens serial port, sets data rate to 9600 bps
  
    pinMode(RED_LED, OUTPUT);
    pinMode(GREEN_LED, OUTPUT);
  
    Serial.println("[Ver. 2.0]");        // Version 2.0
    Serial.println("Hello!");
    Serial.println("*** Welcome to MSP430's Beats project. ***");
    Serial.println();
    Serial.println("[Notice] Use bpm + number's to test.");
    Serial.println("    Have fun :)");   // Ready to command
}

void loop()
{
           
    if(BPM>0)
    {
        BPM_beats(BPM, 1);  // Ex : BPM:60 , beat:4 (=4/4)
    }
    else
    {
        // Active
        digitalWrite(GREEN_LED, HIGH);
        digitalWrite(RED_LED, LOW);
        delay(250);        // wait for command(250 ms)
        time = 0;
    }     
       //noInterrupts();
interrupts();             
    if(Serial.available() > 0)            //Keyin to active
         {              
             longDatas = "\0";           // clear String buffer !
             while(Serial.available() > 0)
               {
                    Data = Serial.read();  // Got what you say                        
                    longDatas = String(longDatas + Data); // Save a char I got
               }
                    longDatas = String(longDatas + ' ');  // Add a null char
              Data_buffer = longDatas;               // Save Datas to buffer
              Serial.println(longDatas);              // String echo!        
              
 //           if (Data_buffer.equals('G') || Data_buffer.equals('g'))  
            if (Data_buffer.substring(0)=="stop ")  
                {
                    Serial.println("BPM normal Stop.");
                    BPM = 0;
                }              
            else if (Data_buffer.substring(0,3)=="BPM" || Data_buffer.substring(0,3)=="bpm")
                {
                     if (Data_buffer.substring(3,8)=="stop " || 
                         Data_buffer.substring(3,8)=="STOP " ||
                         Data_buffer.substring(4,9)=="stop " ||
                         Data_buffer.substring(4,9)=="STOP " )
                     {
                        Serial.println("BPM normal Stop.");
                        BPM = 0;
                     }              
                     else if(Data_buffer.substring(4,5)=="0"||
                             Data_buffer.substring(4,5)=="1"||
                             Data_buffer.substring(4,5)=="2"||
                             Data_buffer.substring(4,5)=="3"||
                             Data_buffer.substring(4,5)=="4"||
                             Data_buffer.substring(4,5)=="5"||
                             Data_buffer.substring(4,5)=="6"||
                             Data_buffer.substring(4,5)=="7"||
                             Data_buffer.substring(4,5)=="8"||
                             Data_buffer.substring(4,5)=="9")
                     {
                         getBPM();      // GET BPM funtion
                         if(BPM < 60 && BPM >0)
                         {   
                             Serial.println("***[WARGING]BPM is too slow ...***");
                             BPM = 0;  
                         }  
                         else if(BPM>300)
                         {
                             Serial.println("***[WARGING]BPM is too fast !!!***");
                             BPM = 0;  
                         }
                     }
                     else
                     { Serial.println( " Error for BPM command ."); }
                     time=0;
                 }
              else
                 {Serial.println("No command.");}
            }
}

void BPM_beats(int BPM, int beats) // Ex : BPM:60 , beat 4
{       
      //  ∥　‥　‥　‥　︱　‥　‥　‥　∥...  => 1 sec long
      //  ("↑" is HIGH triggle, "￣" is keep HIGH, "↓" is LOW triggle, "＿" is keep LOW)
      
      //  ↑￣￣￣￣￣￣￣↓＿＿＿＿＿＿＿↑...  => BPM = 60  (↑to↓ delay 0.5 sec)
      //  ↑￣￣￣↓＿＿＿↑￣￣￣↓＿＿＿↑...  => BPM = 120 (↑to↓ delay 0.25 sec)
      //  ↑￣↓＿↑￣↓＿↑￣↓＿↑￣↓＿↑...  => BPM = 240 (↑to↓ delay 0.125 sec)
      //  ↑↓↑↓↑↓↑↓↑↓↑↓↑↓↑↓↑...  => BPM = 480 (↑to↓ delay 0.0625 sec. Too short !!)
      
//      else
//      {
          int count;
          int delaytime = 1200 / beats / BPM; // ( ( (1 / (BPM / 60) / beats) * 1000) / 2) / 25;
      
          digitalWrite(RED_LED, HIGH);           
          for(count=0; count<delaytime; count++)
          {delay(25);}  //25ms a step =>The Fastest BPM:300 , beat:4
      
          digitalWrite(RED_LED, LOW);                         
          for(count=0; count<delaytime; count++)
          {delay(25);} // 25 + 25 = 50ms
      
          time++;
          time = time % beats;
          if(beats == 1)
          {  Serial.println(time+1);}
          else
          {  Serial.println(time);}
 //     }
}

void getBPM()
{
    Data_buffer.toCharArray(BPM_buffer, Data_buffer.length());
    int count;
                    
    if (Data_buffer.substring(3,5)=="  ")
        {
            Serial.println( " Error !"); 
            BPM_num[0] = 0;
            BPM_num[1] = 0;
            BPM_num[2] = 0;
        }
    else if (Data_buffer.substring(3,4)==" ")
        {
           /*for(count = 4; count<= Data_buffer.length()-2; count++)
              { 
              //Serial.print( BPM_buffer[count]); //Echo BPM num string
           }*/   
              BPM_num[0] = BPM_buffer[4] - 48;
              BPM_num[1] = BPM_buffer[5] - 48;
              BPM_num[2] = BPM_buffer[6] - 48;
           
        }              
    else
        {
           /* for(count = 3; count<= Data_buffer.length()-2; count++)
            { 
              //Serial.print( BPM_buffer[count]); //Echo BPM num string
            } */ 
              BPM_num[0] = BPM_buffer[3] - 48;
              BPM_num[1] = BPM_buffer[4] - 48;
              BPM_num[2] = BPM_buffer[5] - 48;
            
        }                    
//    Serial.println();
    
    if(BPM_num[1]<0)   // if BPM < 10
    {    
         BPM = BPM_num[0];
         Serial.print("Got BPM is :");   
         Serial.println(BPM);
    }
    else if(BPM_num[2]<0)   // if BPM < 100
    {    
         BPM = BPM_num[0]*10 + BPM_num[1];
         Serial.print("Got BPM is :");      
         Serial.println(BPM);
    }
    else                    // If BPM >= 100
    {    
        BPM = BPM_num[0]*100 + BPM_num[1]*10 + BPM_num[2];
        Serial.print("Got BPM is :");        
        Serial.println(BPM);
    }
    
    

}
