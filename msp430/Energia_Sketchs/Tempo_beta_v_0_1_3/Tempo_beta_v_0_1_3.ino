char Data = 0;                    // for got serial data
char char_buffer[8];              // for got BPM time in char
int  BPM_num[4] = {0,0,0,0};      // for got BPM time in int
int  BPM=0;                       // Real BPM number
int  delay_num[3] = {0,0,0};      // for got delay in int
int  delay_time=0;                // Real delay time
int  delay_check=0;               // check delay ?

String longDatas = "\0";  // for saving char "Data"
String Data_buffer = "\0";// for saving String "longDatas"
String AT_Str0 = "at";
String AT_Str1 = "AT";

void setup() 
{
  
    Serial.begin(9600);    // opens serial port, sets data rate to 9600 bps
  
    pinMode(RED_LED, OUTPUT);
    pinMode(GREEN_LED, OUTPUT);
  
    Serial.println("[Ver. 2.0]");        // Version 2.0
    Serial.println("Hello!");
    Serial.println("*** Welcome to MSP430's Beats project. ***");
    Serial.println();
    Serial.println("[Notice] Use bpm + number's to test.");
    Serial.println("    Have fun :)");   // Ready to command
}

void loop()
{
           
    if(BPM>0)
    {
      int delay_count;
        // delay time for shift pitches,
     
        if(delay_check > 0)
        {
           for(delay_count=0; delay_count < delay_time; delay_count++)
              {delayMicroseconds(1000);} // delay (msec)
          
           Serial.println("BPM Recheck!");
           delay_check  = 0;
        }
        else
        {
        // Active : start Beats
          BPM_beats(BPM, 1);  // Ex : BPM:60 , beat:4 (=4/4)
        }
    }
    else
    {
        // Active
        digitalWrite(GREEN_LED, HIGH);
        digitalWrite(RED_LED, LOW);
        delay(250);        // wait for command(250 ms)
        time = 0;
    }     
       //noInterrupts();
interrupts();             
    if(Serial.available() > 0)            //Keyin to active
         {              
             longDatas = "\0";           // clear String buffer !
             while(Serial.available() > 0)
               {
                    Data = Serial.read();  // Got what you say                      
                    longDatas = String(longDatas + Data); // Save a char I got

//                    Serial.print(">>");
//                    Serial.println(Data);       //check
               }
              longDatas = String(longDatas + ' ');  // Add a null char
              Data_buffer = longDatas;               // Save Datas to buffer
              Serial.println(longDatas);              // String echo!        
              
             if (Data_buffer == "00000000 ")  
                {
                    Serial.println("BPM normal Stop.");
                    BPM = 0;
                }              
            else if (Data_buffer.substring(4,5)=="D")  //show Delay time & BPM (ms)
                {
                     getBPM();
                     Serial.println("Get datas :");
                     Serial.println(BPM);
                     Serial.println(delay_time);
                     delay_check = 1;
                 }
              else
                 {
//                   Serial.println("No command.");
                     Serial.println(Data_buffer);    // String echo! 
                 }
           }
}

void BPM_beats(int BPM)
{       
          digitalWrite(RED_LED, HIGH);           
          digitalWrite(GREEN_LED, LOW);
          delay(BPM/4);

          digitalWrite(RED_LED, LOW);      
          digitalWrite(GREEN_LED, HIGH);       
          delay(BPM*3/4);     
}

void getBPM()
{
    Data_buffer.toCharArray(char_buffer, 9);
    int count;
    Serial.println(char_buffer);
    
    BPM_num[0] = char_buffer[0] - 48;
    BPM_num[1] = char_buffer[1] - 48;
    BPM_num[2] = char_buffer[2] - 48;
    BPM_num[3] = char_buffer[3] - 48;
    
   delay_num[0] = char_buffer[5] - 48;
   delay_num[1] = char_buffer[6] - 48;                  
   delay_num[2] = char_buffer[7] - 48;  
   
   BPM =  BPM_num[0] * 1000 + BPM_num[1] * 100 + BPM_num[2] * 10 + BPM_num[3];
   delay_time =  delay_num[0] * 100 + delay_num[1] * 10 + delay_num[2];
}
