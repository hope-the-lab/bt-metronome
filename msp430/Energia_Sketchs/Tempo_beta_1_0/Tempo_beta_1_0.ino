char Data = 0;	// for incoming serial data

String longDatas = "\0";// for saving char "Data"
String Datas = "\0";
String Setting0 = "120";
String Setting1 = "60";
String AT_Str0 = "at";
String AT_Str1 = "AT";

int time = 0;

//volatile int flag0 = LOW;
//volatile int flag1 = LOW;

void setup() 
{
  
  Serial.begin(9600);    // opens serial port, sets data rate to 9600 bps
  
  pinMode(RED_LED, OUTPUT);
  pinMode(GREEN_LED, OUTPUT);
}

void loop()
{
    if (Datas == Setting0)
         { 
           // Active     
            BPM_120();
             digitalWrite(GREEN_LED, LOW);
             digitalWrite(RED_LED, LOW);
         }
    else if (Datas.equals(Setting1))
         {
           // Active
             BPM_60();
             digitalWrite(GREEN_LED, LOW);
             digitalWrite(RED_LED, LOW);
         }       
    else
         {                       
           // Active
           digitalWrite(GREEN_LED, HIGH);
           digitalWrite(RED_LED, LOW);
           delay(2);        // wait for command
           time = 0;           
         }
         
       //noInterrupts();
interrupts();             
            if(Serial.available() > 0)
            {              
               longDatas = "\0";              // clear String !
               while(Serial.available() > 0)
                      {
                        Data = Serial.read();  // say what you got:
//                      delay(1);             // delay for chars to print string.
//            		Serial.print(Data);
                        
                        longDatas = String(longDatas + Data);
                      }
              Datas = longDatas;
              if (Datas.equals(AT_Str0) || Datas.equals(AT_Str1))
              {Serial.println(longDatas);}         // String echo for AT-command
              else
              {Serial.print(longDatas);}         // String echo!
            }
}

void BPM_120()
{
//      int time = 0;
  
      digitalWrite(RED_LED, HIGH);                         
      delay(250);
      digitalWrite(RED_LED, LOW);                         
      delay(250);

 //   if(time == 0)
 //   {
 //     Serial.println("Get BPM : 120");
 //   }
    time++;
    Serial.println(time);
}

void BPM_60()
{
  int count;
    
    digitalWrite(RED_LED, HIGH);  
    
    for(count=500;count>0;count--)    //500ms
    {delayMicroseconds(1000);}
    
    digitalWrite(RED_LED, LOW);     
    
     for(count=500;count>0;count--)  //500ms
    {delayMicroseconds(1000);}
  
  //  if(time == 0)
  //  {
  //    Serial.println("Get BPM : 60");
  //  }
    time++;
    Serial.println(time); 
}
