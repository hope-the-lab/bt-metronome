char Data = 0;                    // for got serial data
char char_buffer[8];              // for got BPM time in char
int  BPM_num[4] = {0,0,0,0};      // for got BPM time in int
int  BPM = 0;                     // Real BPM number
int  BPM_CS = 0;                  // BPM count for keep sync
int  CS_temp = 0;                 // Count Sync memory
int  Syncdelay = 2000;            // Real Sync time
int  check_Sync = 0;              // Sync command check



String longDatas = "\0";      // for saving char "Data"
String Data_buffer = "\0";    // for saving String "longDatas"
String AT_Str0 = "at";
String AT_Str1 = "AT";

void setup() 
{
  
    Serial.begin(9600);    // opens serial port, sets data rate to 9600 bps
  
    pinMode(2, OUTPUT);   // set for Green LED
    pinMode(7, OUTPUT);   // set for DC Mortor
  
    Serial.println("[Ver. beta 1.6.1]");        // Version beta 1.6.1
    Serial.println("Hello!");
    Serial.println("*** Welcome to MSP430's Beats project. ***");
    Serial.println();
    Serial.println("[Notice] wait for S2 & BPM to test.");
    Serial.println("    Have fun :)");   // Ready to command
}

void loop()
{
            digitalWrite(2, HIGH);         // * Green LED pin ON
       
       if(BPM > 0)         // Active Mortor on beats
           {
               digitalWrite(7,  LOW);        // * Mortor pin ON    
               CS_temp = CS_temp - 1;             
             //  Serial.print(CS_temp); // print for check sync
           }         
           
            delay(Syncdelay + 75);      // Delay for LED signal & Mortor active pulse
          
            digitalWrite(7, HIGH);           // * Mortor pin OFF 
            
       if(check_Sync == 1) //When Get Sync command
           {digitalWrite(2,  LOW);}        // * Green LED pin OFF       
           
           delay(Syncdelay + BPM - 75); // Delay for each mode's time control
            
           
          
           
       //noInterrupts();
interrupts();             

         if(Serial.available() > 0) //Keyin to active
                  {              
                      longDatas = "\0";           // clear String buffer !
                      while(Serial.available() > 0)
                        {
                             Data = Serial.read();  // Got what you say                      
                             longDatas = String(longDatas + Data); // Save a char I got
                        }
                       longDatas = String(longDatas + ' ');  // Add a null char
                       Data_buffer = longDatas;               // Save Datas to buffer
                       Serial.println(longDatas);              // String echo!        
              
                       // Get commands
                      if (Data_buffer.substring(0,1)=="0")  //Sync mode & stop beats
                         {
                             BPM = 0;
                             check_Sync = 1;
                             Syncdelay = 2000;
                           
                             
                             Serial.println("BPM Stop & Resync.");
                         }    
                      else if (Data_buffer.substring(0,1)=="S")  //BPM beats mode
                         {
                              getBPMdata();
                              CS_temp = BPM_CS - 1;
                              Syncdelay = 0;
                              
                              Serial.print("Get BPM's delaytime : ");
                              Serial.print(BPM);
                              Serial.println(" ms");
                              Serial.print("Every ");
                              Serial.print(BPM_CS);
                              Serial.println(" beats to keep sync.");
                         }
                     else if (Data_buffer.substring(0,1)=="i") // Initialize mode  
                         {               
                             BPM = 0;  
                             check_Sync = 0;       
                             Syncdelay = 125;
                             
                             Serial.println("* Initialize *");
                         }       
                    else if(Data_buffer.substring(0,4)=="help")
                         {
                                Serial.print("*BPM = ");
                                Serial.println(BPM);
                                Serial.print("*BPM_CS = ");
                                Serial.println(BPM_CS);
                                Serial.print("*CS_temp = ");
                                Serial.println(CS_temp);
                                Serial.print("*Syncdelay = ");
                                Serial.println(Syncdelay);
                                Serial.print("*check_Sync = ");
                                Serial.println(check_Sync);
                         }
                         
                      else  // Not commands
                          {
         //                   Serial.println("Not command.");
                              Serial.println(Data_buffer);    // String echo! 
                          }
                       
                    }              
           
    if(CS_temp == 0 && BPM_CS > 0)
           {
                 Serial.println("Check !");
                 CS_temp = BPM_CS;
           } 

           
}


void getBPMdata()
{
    Data_buffer.toCharArray(char_buffer, 8);
//    int count;
    Serial.println(char_buffer);
    
    BPM_num[0] = char_buffer[1] - 48;
    BPM_num[1] = char_buffer[2] - 48;
    BPM_num[2] = char_buffer[3] - 48;
    BPM_num[3] = char_buffer[4] - 48;
  
   BPM =  BPM_num[0] * 1000 + BPM_num[1] * 100 + BPM_num[2] * 10 + BPM_num[3];
   BPM_CS = char_buffer[6] - 48;
}
