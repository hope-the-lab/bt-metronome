char Data = 0;                    // for got serial data
char char_buffer[8];              // for got BPM time in char
int  BPM_num[4] = {0,0,0,0};      // for got BPM time in int
int  BPM = 0;                     // Real BPM number
char Sync = '0';                  // Real delay time

String longDatas = "\0";  // for saving char "Data"
String Data_buffer = "\0";// for saving String "longDatas"
String AT_Str0 = "at";
String AT_Str1 = "AT";

void setup() 
{
  
    Serial.begin(9600);    // opens serial port, sets data rate to 9600 bps
  
    pinMode(2, OUTPUT);   // set for Green LED
    pinMode(7, OUTPUT);   // set for DC Mortor
  
    Serial.println("[Ver. beta 1.5.1]");        // Version beta 1.5.1
    Serial.println("Hello!");
    Serial.println("*** Welcome to MSP430's Beats project. ***");
    Serial.println();
    Serial.println("[Notice] wait for S2 & BPM to test.");
    Serial.println("    Have fun :)");   // Ready to command
}

void loop()
{
           
    if(Sync == '1')  //Active Sync 2 sec
    {
        if(BPM > 0)//Active BPM !
        {           
          BPM_beats(BPM);  // Ex : BPM:60 , beat:4 (=4/4)         
        }  
        else       //Sync but no BPM output.
        {
          digitalWrite(2, HIGH);
          delay(1000);
          digitalWrite(2, LOW);
          delay(1000);          
        }
      
    }
    else  //STOP Sync & BPM
    {
        // Active
        digitalWrite(2, HIGH);
        digitalWrite(7, LOW);
        delay(250);        // wait for command(250 ms)
    }     
       //noInterrupts();
interrupts();             
    if(Serial.available() > 0)            //Keyin to active
         {              
             longDatas = "\0";           // clear String buffer !
             while(Serial.available() > 0)
               {
                    Data = Serial.read();  // Got what you say                      
                    longDatas = String(longDatas + Data); // Save a char I got

//                    Serial.print(">>");
//                    Serial.println(Data);       //check
               }
              longDatas = String(longDatas + ' ');  // Add a null char
              Data_buffer = longDatas;               // Save Datas to buffer
              Serial.println(longDatas);              // String echo!        
              
             if (Data_buffer == "00000000 ")  
                {
                    Serial.println("BPM normal Stop.");
                    BPM = 0;
                    Sync = '1';    // Stop BPM & Back to Sync mode.
                }  
             else if (Data_buffer.substring(0,2)=="S2")
                {    Sync = '1'; } // Go to Sync mode.           
             else if (Data_buffer.substring(0,1)=="S")  //show Delay time & BPM (ms)
                {
                     getBPM();
                     Serial.println("Get datas :");
                     Serial.println(BPM);
                }            
             else
                 {
//                   Serial.println("No command.");
                     Serial.println(Data_buffer);    // String echo! 
                 }
           }
}

void BPM_beats(int BPM)
{       
      if(BPM <= 300)                      // BPM >= 200
      {
          digitalWrite(7, HIGH);           // Mortor  ON
          digitalWrite(2, HIGH);           // Green ON
          delay(BPM*4/16);
          
          digitalWrite(7, LOW);            // Mortor   OFF 
          digitalWrite(2, LOW);           // Green OFF       
          delay(BPM*12/16);     
      }
      else if(BPM <= 400)                      // BPM >= 150
      {
          digitalWrite(7, HIGH);           // Mortor  ON
          digitalWrite(2, HIGH);           // Green ON
          delay(BPM*3/16);
          
          digitalWrite(7, LOW);            // Mortor   OFF 
          digitalWrite(2, LOW);           // Green OFF       
          delay(BPM*13/16);     
      }
      else if (BPM <= 600)              // BPM >= 100
      {
          digitalWrite(7, HIGH);           // Mortor  ON
          digitalWrite(2, HIGH);           // Green ON
          delay(BPM*2/16);
          
          digitalWrite(7, LOW);            // Mortor   OFF 
          digitalWrite(2, LOW);           // Green OFF       
          delay(BPM*14/16);     
      }
      else                            // BPM < 100
      {
          digitalWrite(7, HIGH);           // Mortor  ON
          digitalWrite(2, HIGH);           // Green ON
          delay(BPM*1/16);
          
          digitalWrite(7, LOW);            // Mortor   OFF 
          digitalWrite(2, LOW);           // Green OFF       
          delay(BPM*15/16);
      }          
}

void getBPM()
{
    Data_buffer.toCharArray(char_buffer, 6);
//    int count;
    Serial.println(char_buffer);
    
    BPM_num[0] = char_buffer[1] - 48;
    BPM_num[1] = char_buffer[2] - 48;
    BPM_num[2] = char_buffer[3] - 48;
    BPM_num[3] = char_buffer[4] - 48;
  
   BPM =  BPM_num[0] * 1000 + BPM_num[1] * 100 + BPM_num[2] * 10 + BPM_num[3];
   //delay_time =  delay_num[0] * 100 + delay_num[1] * 10 + delay_num[2];
}
