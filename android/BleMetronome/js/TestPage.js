import React, {Component} from 'react';
import {View, Text } from 'react-native';
import {Router , Actions}from 'react-native-router-flux';
import EStyleSheet from 'react-native-extended-stylesheet';
import * as BleAction from './action/BleAction';
const styles=EStyleSheet.create({});
export default class TestPage extends Component {
constructor(){
	super();
}
componentWillMount(){
	BleAction.ConnectPeripheral();
}
componentWillUnmount(){
	BleAction.DisconnectPeripheral();
	Actions.pageOne();
	BleAction.ResetBle();
}
render() {
  return (
	<View>
	  
	</View>
  )
}
}