import React, {Component} from 'react';
import {View, Text } from 'react-native';
import {Router , Actions}from 'react-native-router-flux';
import EStyleSheet from 'react-native-extended-stylesheet';
import * as BleAction from './action/BleAction';
import ConnectDevicesBlock from './component/ConnectDevicesBlock';
import BpmController from './component/BpmController';
import PeripheralModal from './component/PeripheralModal';

import BleStateStore from './store/BleStateStore';
const styles=EStyleSheet.create({
    DeviceBlock:{
        height:'20%',
        width:'100%',
        backgroundColor:'#828282',
    },
    MetronomeBlock:{
        flexDirection:'column',
        height:'80%',
    },
});
export default class ConnectPage extends Component {
constructor(props){
	super(props);
    this.handleStateUpdate=this.handleStateUpdate.bind(this);
    this.state={
        devicesList:[],
    }
}
componentWillMount(){
	BleAction.ConnectPeripheral();
    var timer = 2000;
    BleAction.startTraceLatency(timer);
    BleStateStore.on('connectIsUpdate',this.handleStateUpdate);
}
componentWillUnmount(){
    BleStateStore.removeListener('connectIsUpdate',this.handleStateUpdate);
    BleStateStore.StopTraceLatency();
	BleAction.DisconnectPeripheral();
	//Actions.pageOne();
    //console.log(Actions);
	BleAction.ResetBle();
}
handleStateUpdate(){
    var list = BleStateStore.getDevicesList();
    this.setState({devicesList:list});
	}
render() {
  return (
	<View>
      <PeripheralModal devicesList={this.state.devicesList} />
      <View style={styles.DeviceBlock}><ConnectDevicesBlock devicesList={this.state.devicesList} /></View>
      <View style={styles.MetronomeBlock}>
            <BpmController/>
      </View>
	</View>
  )
}
}