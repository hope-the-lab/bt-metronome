import dispatcher from "../store/dispatcher";

export function InitBle(){
	dispatcher.dispatch({
		type:'INIT_BLE'
	});
}
export function ResetBle(){
	dispatcher.dispatch({
		type:'RESET_BLE'
	});
}
export function StartScan(){
	dispatcher.dispatch({
		type:'START_SCAN'
	});
}
export function StopSacn(){
	dispatcher.dispatch({
		type:'STOP_SCAN'
	});
}
export function CleanPeripheral(){
	dispatcher.dispatch({
		type:'ClEAN_PERIPHERAL'
	});
}
export function ConnectPeripheral(){
	dispatcher.dispatch({
		type:'CONNECT_PERIPHERAL'
	});
}
export function DisconnectPeripheral(){
	dispatcher.dispatch({
		type:'DISCONNECT_PERIPHERAL'
	});
}
export function RequestPeripheralContent(index){
    dispatcher.dispatch({
        type:'REQUEST_PERRIPHERAL_CONTENT',
        index:index,
    });
}
export function sendBPMtoPeripheral(value,bpm){
    dispatcher.dispatch({
        type:'SEND_BPM_TO_PERIPHERAL',
        time:value,
        bpm:bpm,
    });
}
export function fireStop(){
	dispatcher.dispatch({
		type:'STOP_PERIPHERAL',
	});
}
export function startTraceLatency(timer){
    dispatcher.dispatch({
        type:'START_TRACE_LATENCY',
        timer:timer,
    });
}