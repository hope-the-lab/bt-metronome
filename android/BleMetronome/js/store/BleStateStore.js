import { EventEmitter } from "events";
import {NativeAppEventEmitter} from 'react-native';
import dispatcher from "./dispatcher";
import BleManager from 'react-native-ble-manager';

var PeripheralList=[];
var ConnectList=[];
var PeripheralInfoList=[];
var OfflineList=[];
var InternalTracer=null;
const serviceUUIDforLatancyTest = '00001800-0000-1000-8000-00805F9B34FB';
const characteristicUUIDforLatancyTest = '00002A00-0000-1000-8000-00805F9B34FB';
const serviceUUID = '0000FFE0-0000-1000-8000-00805F9B34FB';
const characteristicUUID = '0000FFE1-0000-1000-8000-00805F9B34FB';
class BleStateStore extends EventEmitter {
	constructor() {
		 super();
		this.handleDiscoverPeripheral = this.handleDiscoverPeripheral.bind(this);
        this.PaddingLeft =this.PaddingLeft.bind(this);
	 }
	InitBle(){
		 BleManager.enableBluetooth().then(() => {
			 // Success code
			 BleManager.start({showAlert: false});
			 console.log('The bluetooh is already enabled or the user confirm');
			 
			 
		 })
			 .catch((error) => {
			 // Failure code
			 console.log('The user refuse to enable bluetooth');
		 });
	 }
	ResetBle(){
		PeripheralInfoList=[];
		this.iniConnectList()
		this.CleanPeripheral()
		this.StopScan();
	}
	getConnectList(){
		return ConnectList;
	}
	iniConnectList(){
		ConnectList=[];
	}
	addToList(id,name){
		ConnectList.push({
			id:id,
			name:name,})
	}
	delformList(id){
		var index = ConnectList.find(function(el,index){
			if(el.id == id){
				return index;
			}
			
		});
		ConnectList.splice(index);
	}
	StartScan(){
			BleManager.scan([], 30, true)
				.then(() => {
				// Success code
				console.log('Scan started');
				NativeAppEventEmitter.addListener('BleManagerDiscoverPeripheral', this.handleDiscoverPeripheral );
			});
	}
	StopScan(){
		BleManager.stopScan()
			.then(() => {
			console.log('Scan stopped');
			NativeAppEventEmitter.removeListener('BleManagerDiscoverPeripheral', this.handleDiscoverPeripheral );
		});
	}
	handleDiscoverPeripheral(data){
		PeripheralList.push(data);
		this.emit('ScanPeripheralUpdate',PeripheralList);
	}
	CleanPeripheral(){
		PeripheralList = [];
		this.emit('CleanScanPeripheral',PeripheralList);
	}
	ConnectPeripheral(){
        var base = this;
        var size = ConnectList.length;
        var index = 0;
		ConnectList.map((a,i)=>{
			BleManager.connect(a.id).then((peripheralInfo)=>{
                var peripheralForm = {
                    id:peripheralInfo.id,
                    name:peripheralInfo.name,
                    status:'Connected',
                    lastData:'',
                    offlineTry:0,
                    latency:0
                }
				PeripheralInfoList.push(peripheralForm);
                base.emit('connectIsUpdate',null);
                index++;
                if(index==size){
                    base.emit('StatusMsgUpdate','All devices are connected !');
                }
			}).catch(()=>{
                base.emit('StatusMsgUpdate',a.name+' connect fail !');
                OfflineList.push(PeripheralInfoList.splice(i,1)[0]);
            });
		});	
	}
    StartTraceLatency(timer){
        var base = this;
        if(InternalTracer){clearInterval(InternalTracer)};
        InternalTracer = setInterval(function(){
            var dateflagSTART = Date.now();
            PeripheralInfoList.map((a,i)=>{
                BleManager.read(a.id,serviceUUIDforLatancyTest,characteristicUUIDforLatancyTest)
                    .then((data)=>{
                    var dateflagEND = Date.now();
                    a.latency = parseInt((dateflagEND-dateflagSTART)/2);
                    a.offlineTry = 0;
                    base.emit('connectIsUpdate',null);
                }).catch(()=>{
                    a.offlineTry++;
                    if(a.offlineTry>=10){
                        base.emit('StatusMsgUpdate',a.name+' is lost !');
                        OfflineList.push(PeripheralInfoList.splice(i,1)[0]);
//                      base.emit('OfflineUpdate',base.getOfflineList);
                    }
                    base.emit('connectIsUpdate',null);
                });
            });
        },timer)
    }
    StopTraceLatency(){
        if(InternalTracer){clearInterval(InternalTracer)};
    }
	DisconnectPeripheral(){
		ConnectList.map((a,i)=>{
			BleManager.disconnect(a.id).then(()=>{
				console.log('Disconnected');
			});
		});
	}
    getDevicesList(){
        return PeripheralInfoList;
    }
    getOfflineList(){
        return OfflineList;
    }
    SendPeripheralContent(index){
        this.emit('showPeripheralcontent',index);
    }
    
    PaddingLeft(str,lenght){
    var base = this;
	if(str.length >= lenght)
	return str;
	else
	return base.PaddingLeft("0" +str,lenght);
    }
    
    StartToSend(time,bpm){
        var base = this;
        PeripheralInfoList.map((a,i)=>{
            a.lastData = this.PaddingLeft(time.toString(),4)+'D'+this.PaddingLeft(a.latency.toString(),3);
        })
        return base.SendBPMtoPeripheral(bpm);
    }
    
	SendBPMtoPeripheral(bpm){
        var base = this;
        base.emit('StatusMsgUpdate','Sending bpm '+bpm+' ...');
        var size = PeripheralInfoList.length;
        var index = 0;
        PeripheralInfoList.map((a,i)=>{
            var data = a.lastData
            BleManager.write(a.id,serviceUUID,characteristicUUID,data,8).then(()=>{
                index++;
                if(index==size){
                    base.emit('StatusMsgUpdate','we are now at '+ bpm);
                }
/*                BleManager.read(a.id,serviceUUID,characteristicUUID).then((data)=>{
//                    index++;
//                if(index==size){
//                    base.emit('StatusMsgUpdate','we are now at '+ bpm);
//                }
//                }).catch(()=>{
//                    base.emit('StatusMsgUpdate','read unsuccessful please try again !');
//               });
*/ 
            }).catch(()=>{
                base.emit('StatusMsgUpdate','sent unsuccessful please try again !');
            });
            
        });
    }
    SendStop(){
        var base = this;
        var size = PeripheralInfoList.length;
        var index = 0;
        base.emit('StatusMsgUpdate','Try to stop all devices');
        PeripheralInfoList.map((a,i)=>{
            BleManager.write(a.id,serviceUUID,characteristicUUID,'00000000',8).then(()=>{
                index++;
                a.lastData='00000000';
                if(index==size){
                    base.emit('StatusMsgUpdate','Now stop');
                }
            });
        });
    }
	handleActions(action) {
		 switch(action.type){
			 case 'INIT_BLE':
			 	this.InitBle();
			 	break;
			 case 'RESET_BLE':
				 this.ResetBle();
				 break;
			 case 'ClEAN_PERIPHERAL':
				 this.CleanPeripheral();
				 break;
			 case 'START_SCAN':
				this.StartScan();
				 break;
			 case 'STOP_SCAN':
				 this.StopScan();
				 break;
			 case 'CONNECT_PERIPHERAL':
				 this.ConnectPeripheral();
				 break;
			 case 'DISCONNECT_PERIPHERAL':
				 this.DisconnectPeripheral();
				 break;
             case 'REQUEST_PERRIPHERAL_CONTENT':
                 this.SendPeripheralContent(action.index);
                 break;
             case 'SEND_BPM_TO_PERIPHERAL':
                 this.StartToSend(action.time,action.bpm);
                 break;
             case 'STOP_PERIPHERAL':
                 this.SendStop();
                 break;
             case 'START_TRACE_LATENCY':
                 this.StartTraceLatency(action.timer);
                 break;
		 }
    }
 }
const blestore = new BleStateStore;
dispatcher.register(blestore.handleActions.bind(blestore));
export default blestore;