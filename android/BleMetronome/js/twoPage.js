import React, {Component} from 'react';
import {View, Text } from 'react-native';
import {Actions}from 'react-native-router-flux';
import ListScanItem from './component/listScanItem';
import GoTestBtn from'./component/GoTestBtn';
import GoConnectBtn from'./component/GoConnectBtn';
import * as BleAction from './action/BleAction';
import EStyleSheet from 'react-native-extended-stylesheet';
import BleStateStore from './store/BleStateStore';
const styles=EStyleSheet.create({
	listContainer:{
		height:'80%',
	},
	btnBlock:{
		height:'20%',
		backgroundColor:'#565656',
		flexDirection:'row',
	},
	HalfVeiw:{
		flex:1,
		
	}
});

export default class PageTwo extends Component {
componentWillMount(){
	
}
componentWillUnmount(){
	BleAction.CleanPeripheral();
	BleAction.StopSacn();
}
render() {
  return (
	<View>
	  <View style={styles.listContainer}>
	  	<ListScanItem />
	  </View>
	  	<View style={styles.btnBlock}>
	        <GoConnectBtn style={styles.HalfVeiw} title="connect" />
	  	</View>
	</View>
  )
}
}