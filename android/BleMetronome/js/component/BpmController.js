'use strict';
import React, { Component } from 'react';
import {View, Text,TextInput,ScrollView } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import BleStateStore from '../store/BleStateStore';
import * as BleAction from '../action/BleAction';

import DeviceRect from'./DeviceRect';
import StatusText from'./StatusText';
import SendBtn from'./SendBtn';
import StopBtn from'./StopBtn';

const styles=EStyleSheet.create({
    viewBlock:{
        alignItems:'center',
        flexDirection:'column',
        marginTop:40,
    },
    inputBlock:{
        width:'60%',
        flexDirection:'row',
        alignItems:'center',
       
    },
    input:{
        flex:1,
        textAlign:'center',
        fontSize:80,
        lineHeight:60,
        padding:0,
    },
    status:{
        marginTop:40,
        width:'90%',
        flex:1,
        height:30,
        alignItems:'center',
    },
    statusText:{
        fontSize:20,
    },
    btnView:{
        flex:1,
        width:120,
        flexDirection:'row',
        height:70,
        marginTop:40,
        alignItems:'center',
    },
    stopView:{
        flex:1,
        width:120,
        flexDirection:'row',
        height:30,
        marginTop:20,
        alignItems:'center',
    },
});
export default class BpmController extends Component {
	constructor(props){
		super(props);
        this.updateNum = this.updateNum.bind(this);
        this.state={
            bpm:100,
            BpmisActive:true,
        }
	}
	componentWillMount(){
	}
	componentWillUnmount(){
	}
    updateNum(target){
        target=target?target:0;
        var target_int =parseInt(target);
        this.setState({bpm:target_int});
        if(target_int>=60&&target_int<=250){
            this.setState({BpmisActive:true});
        }else{
            this.setState({BpmisActive:false});
        }
    }
	render(){
		return(
            <View style={styles.viewBlock}>
                <View style={styles.inputBlock}>
                    <TextInput keyboardType="numeric" style={styles.input} onChangeText={(target)=>{this.updateNum(target)}}>{this.state.bpm}</TextInput>
                </View>
                <Text>ENTER BPM value between 60 to 250</Text>
                <View style={styles.status}><StatusText style={styles.statusText} bpm={this.state.bpm}/></View>
                <View style={styles.btnView}><SendBtn bpm={this.state.bpm} BpmisActive={this.state.BpmisActive}/></View>
                <View style={styles.stopView}><StopBtn /></View>
            </View>
		)
	}
}