import React, { Component } from 'react';
import { View, Text } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Actions } from 'react-native-router-flux';

const styles = EStyleSheet.create({
	frame:{
		padding:20,
		paddingTop:40,
		height:'75%'
	},
	h1:{
		fontSize:20,
	},
	bottom:{
		bottom: 0,
		position: 'absolute',
		textAlign: 'center',
		width:'100% - 40'
	}
})

export default class IntroInfo extends Component {
  render() {
    return (
      <View style={styles.frame}>
        
		<Text style={styles.h1}>Welcome to BLE Metronome App</Text>
		<Text>
		   {'\n\n'}
			Please make sure your buletooth is supported by Low Energy specification to use . {'\n'}
			good luck you guys and have a nice day !
		</Text>
		<Text style={styles.bottom}>click 'Scan' if you want to start</Text>
      </View>
    );
  }
}