'use strict';
import React, { Component } from 'react';
import {View, Text,ScrollView } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import BleStateStore from '../store/BleStateStore';
import * as BleAction from '../action/BleAction';

import PeripheralListItems from'./PeripheralListItems';

var isSelectList=[];
export default class ListScanItem extends Component {
	constructor(props){
		super(props);
		this.handleStateUpdate = this.handleStateUpdate.bind(this);
		var base = this;
		var somelist = [];
		this.state={
			somelist:somelist,
			isSelectList:isSelectList
		}
	}
	componentWillMount(){
		BleStateStore.on('ScanPeripheralUpdate',this.handleStateUpdate);
	}
	componentWillUnmount(){
		BleStateStore.removeListener('ScanPeripheralUpdate',this.handleStateUpdate);
	}
	handleStateUpdate(list){
		this.setState({somelist:list});
	}
	ListLooper(list){
		return(
			list.map((a,index)=>{
				return(
					<PeripheralListItems  key={index} name={a.name} id={a.id} />
				)
			})
		)
	}
	render(){
		return(
			<ScrollView>
				{this.ListLooper(this.state.somelist)}
			</ScrollView>
		)
	}
}