'use strict';
import React, { Component } from 'react';
import {View, Text , TouchableHighlight} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import BleStateStore from '../store/BleStateStore';
import * as BleAction from '../action/BleAction';

const styles=EStyleSheet.create({
    text:{
        color:'#fff',
        fontSize:30,
    },
    btn:{
        backgroundColor:'#606060',
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:15,
    }
});

export default class SendBtn extends Component {
	constructor(props){
		super(props);
        this.fireBPM=this.fireBPM.bind(this);
        this.CalculateBpmToMinorsecond=this.CalculateBpmToMinorsecond.bind(this);
        this.state={
            status:'Status',
        }
	}
	componentWillMount(){
	}
	componentWillUnmount(){
	}
    CalculateBpmToMinorsecond(bpm){
        var value = parseInt(60000/bpm).toString();
        return value;
    }
    fireBPM(){
        var bpm = this.props.bpm;
        if(this.props.BpmisActive){  
            var value = this.CalculateBpmToMinorsecond(bpm);
            BleAction.sendBPMtoPeripheral(value,bpm);
        }else{
            BleStateStore.emit('StatusMsgUpdate',bpm+' is not a correct value !');
        }
    }
	render(){
		return(
			<TouchableHighlight style={[styles.btn,]} onPress={()=>{this.fireBPM()}}>
                <View style={{flex:1,justifyContent:'center',}}>
                    <Text style={styles.text}>Send</Text>
                </View>
            </TouchableHighlight>
		)
	}
}