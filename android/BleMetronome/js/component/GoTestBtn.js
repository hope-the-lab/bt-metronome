'use strict';

import React, { Component } from 'react';
import { View, Text } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Actions } from 'react-native-router-flux';
import * as BleAction from '../action/BleAction';
import BleStateStore from '../store/BleStateStore';

var styles = EStyleSheet.create({
	btn:{
		textAlign:'center',
		padding:10,
		marginTop:25,
		color:'#fff',
		width:'50%',
		borderRadius:20,
		fontSize:20
	},
	container:{
		alignItems:'center',
	}
});
var btnColorDisable = {backgroundColor:'#cecece'}
var btnColor ={backgroundColor:'#353535'};

export default class GoTestBtn extends Component {
	constructor(props){
		super(props);
		this.btnPress = this.btnPress.bind(this);
		this.handleStateUpdate =this.handleStateUpdate.bind(this);
		this.state = {
			btnColor:{
				backgroundColor:'#353535'
			},
			btnState:true
		};
	}
	
	componentWillMount(){
		BleStateStore.on('CleanScanPeripheral',this.handleStateUpdate);
	}
	
	componentWillUnmount(){
		BleStateStore.removeListener('CleanScanPeripheral',this.handleStateUpdate);
	}
	
	handleStateUpdate(){
		this.setState({btnColor:btnColor,btnState:true});
	}
	
  btnPress(fn){
	  if(this.state.btnState){
		  this.setState({btnColor:btnColorDisable,btnState:false});
		  fn();
	  }
  }
  render() {
    return (
		<View style={styles.container}>
        	<Text style={[styles._btn,this.state.btnColor]} onPress={()=>this.btnPress(Actions.TestPage)}>{this.props.title}</Text>
		</View>
    );
  }
}