'use strict';
import React, { Component } from 'react';
import {View, Text,TouchableHighlight } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import BleStateStore from '../store/BleStateStore';
import * as BleAction from '../action/BleAction';

const styles={
	container:{
		flex:1,
		flexDirection:'row',
		padding:20,
		height:80,
		borderBottomWidth :1,
		borderBottomColor:'#707070',
	},
	name:{
		flex:1,
		height:40
	},
	id:{
		flex:2,
		height:40
	},
	text:{
		marginTop:10
	},
	select:{
		flex:1,
	},
	selectBtn:{
		height:40,
		textAlign:'center',
		alignItems:'center',
		color:'#ffffff',
		paddingTop:10,
		paddingBottom:10,
		borderRadius:10,
	},
	btnContainerNotSelect:{
		backgroundColor:'#eee',
	},
	btnContainerIsSelect:{
		backgroundColor:'#ffffff',
	},
	btnNotSelect:{
		backgroundColor:'#6090b7',
	},
	btnIsSelect:{
		backgroundColor:'#37ff00',
		color:'#000',
	},
}

export default class PeripheralListItems extends Component {
	constructor(props){
		super(props);
		this.cilckPeripheral=this.cilckPeripheral.bind(this);
		this.state={
			btnContainerStyle:styles.btnContainerNotSelect,
			isSelect:false,
			selectText:'select',
			btnStyle:styles.btnNotSelect,
		}
	}
	componentWillMount(){
	}
	componentWillUnmount(){
	}
	cilckPeripheral(){
		var isSelect = !this.state.isSelect;
		this.setState({isSelect:isSelect});
		if(isSelect){
			this.setState({
				btnContainerStyle:styles.btnContainerIsSelect,
				selectText:'is selected',
				btnStyle:styles.btnIsSelect,
			});
			BleStateStore.addToList(this.props.id,this.props.name);
		}else{
			this.setState({
				btnContainerStyle:styles.btnContainerNotSelect,
				selectText:'select',
				btnStyle:styles.btnNotSelect,
			});
			BleStateStore.delformList(this.props.id);
		}
	}
	render(){
		return(
			<TouchableHighlight onPress={()=>this.cilckPeripheral()}>
				<View style={[styles.container,this.state.btnContainerStyle]}>
					<View style={styles.name}><Text style={styles.text}>{this.props.name}</Text></View>
					<View style={styles.id}><Text style={styles.text}>{this.props.id}</Text></View>
					<View style={styles.select}><Text style={[styles.selectBtn,this.state.btnStyle]}>{this.state.selectText}</Text></View>
				</View>
			</TouchableHighlight >
		)
	}
}