'use strict';
import React, { Component } from 'react';
import {View, Text,ScrollView } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import BleStateStore from '../store/BleStateStore';
import * as BleAction from '../action/BleAction';

import DeviceRect from'./DeviceRect';

const styles=EStyleSheet.create({
    veiwBlock:{
        flex:1,
        flexDirection: 'row',
        alignItems: 'center',
        margin:0,
        padding:0,
    }
});
export default class ConnectDevicesBlock extends Component {
	constructor(props){
		super(props);
        this.state={
            devicesList:[]
        }
	}
	componentWillMount(){
	}
	componentWillUnmount(){
	}
	ListLooper(list){
		return(
			list.map((a,i)=>{
                var color = (function(){
                    if(a.offlineTry==0&&a.latency<100){
                        return '#75fb6e';
                    }else if(a.offlineTry==0&&a.latency>=100){
                        return '#f4fb26';
                    }else{
                        return '#e00000';
                    }
                })()
                    ;
				return(
                    <DeviceRect key={i} index={i} name={a.name} rectColor={{backgroundColor:color}} id={a.id} latency={a.latency}/>
				)
			})
		)
	}
	render(){
		return(
			<ScrollView horizontal={true}>
                <View  style={styles.veiwBlock}>{this.ListLooper(this.props.devicesList)}</View>
			</ScrollView>
		)
	}
}