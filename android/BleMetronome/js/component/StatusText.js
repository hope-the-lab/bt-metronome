'use strict';
import React, { Component } from 'react';
import {View, Text } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import BleStateStore from '../store/BleStateStore';
import * as BleAction from '../action/BleAction';

const styles=EStyleSheet.create({
    text:{
        color:'#fff',
    }
    
});

export default class StatusText extends Component {
	constructor(props){
		super(props);
        this.messageUpdate=this.messageUpdate.bind(this);
        this.state={
            status:'Waiting for devices',
        }
	}
	componentWillMount(){
        BleStateStore.on('StatusMsgUpdate',this.messageUpdate);
	}
	componentWillUnmount(){
        BleStateStore.removeListener('StatusMsgUpdate',this.messageUpdate);
	}
    messageUpdate(msg){
        this.setState({status:msg});
    }
	render(){
		return(
			<Text style={this.props.style}>{this.state.status}</Text>
		)
	}
}