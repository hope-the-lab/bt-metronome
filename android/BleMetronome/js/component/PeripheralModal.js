'use strict';

import React, { Component } from 'react';
import { View, Text ,Modal, ScrollView,TouchableHighlight } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Actions } from 'react-native-router-flux';
import * as BleAction from '../action/BleAction';
import BleStateStore from '../store/BleStateStore';

var styles = EStyleSheet.create({
    frame:{
        width:'80%',
        maxHeight:'80%',
        top:'7%',
        left:'10%',
        backgroundColor:'rgba(0,0,0,0.7)',
        borderRadius:10,
    },
    tilte:{
        fontSize:30,
        textAlign:'center',
        marginBottom:20,
    },
    textW:{
        color:'#fff',
        fontSize:17,
        marginBottom:10,
    },
    textB:{
        color:'#000',
        fontSize:20,
    },
    btn:{
        paddingHorizontal :20,
        paddingVertical:5, 
        backgroundColor:'#ffffff',
        borderRadius:10,
    }
});
export default class PeripheralModal extends Component {
	constructor(props){
		super(props);
        this.CloseModal=this.CloseModal.bind(this);
        this.OpenModal=this.OpenModal.bind(this);
        this.getDeviceNow=this.getDeviceNow.bind(this);
        this.state={
            modalVisible:false,
            index:-1,
        }
        
	}
	
	componentWillMount(){
	   BleStateStore.on('showPeripheralcontent',this.OpenModal);	
	}
	componentWillUnmount(){
        BleStateStore.removeListener('showPeripheralcontent',this.OpenModal);
	}
	handleStateUpdate(){

	}
    OpenModal(index){
        this.setState({modalVisible:true});
        this.setState({index:index});
    }
    CloseModal(){
        this.setState({modalVisible:false});
    }
    getDeviceNow(arg,listNow){
        var deviceNow  = listNow[this.state.index];
        if(deviceNow){
            switch (arg){
                case 'id' :
                    return deviceNow.id;
                case 'name' :
                    return deviceNow.name;
                case 'latency' :
                    return deviceNow.latency;
                case 'status' :
                    return deviceNow.status;
                case 'lastData' :
                    return deviceNow.lastData;
                default:
                    return 'NaN';
            }
        }else{
            return 'NaN';
        }
    }
  render() {
    return (
		<Modal
            animationType={"fade"}
            transparent={true}
            visible={this.state.modalVisible}
            onRequestClose={() => {this.setState({'modalVisible':false})}}
        >
            <View style={styles.frame}>
                <ScrollView>
                <View style={{padding:20}}>
                    <Text style={[styles.textW,styles.tilte]}>{this.getDeviceNow('name',this.props.devicesList)}</Text>
                    <Text style={[styles.textW,]}>MAC address : {this.getDeviceNow('id',this.props.devicesList)}</Text>
                    <Text style={[styles.textW,]}>RSI : </Text>
                    <Text style={[styles.textW,]}>Status : {this.getDeviceNow('status',this.props.devicesList)}</Text>
                    <Text style={[styles.textW,]}>Data : {this.getDeviceNow('lastData',this.props.devicesList)}</Text>
                    <Text style={[styles.textW,]}>Latency : {this.getDeviceNow('latency',this.props.devicesList)}</Text>
                    <View style={{alignItems:'center',marginTop:30,}}>
                        <TouchableHighlight style={styles.btn} onPress={()=>{this.CloseModal()}}>
                            <Text style={[styles.textB,]}>Close</Text>
                        </TouchableHighlight>
                    </View>
                </View>
                </ScrollView>
            </View>
		</Modal>
    );
  }
}