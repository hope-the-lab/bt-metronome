'use strict';
import React, { Component } from 'react';
import {View, Text,TouchableHighlight } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import BleStateStore from '../store/BleStateStore';
import * as BleAction from '../action/BleAction';

const styles=EStyleSheet.create({
    veiwBlock:{
        width:'20%',
    },
    rect:{
        width:15,
        height:15,
        margin:10,
//        backgroundColor:'#88f324',
    },
    rectBlock:{
        paddingTop:'3%',
        paddingBottom:'3%',
        justifyContent:'center',
        alignItems: 'center',
    },
    text:{
        color:'#fff',
    }
    
});

export default class DeviceRect extends Component {
	constructor(props){
		super(props);
        this.requestContent = this.requestContent.bind(this);

	}
	componentWillMount(){
	}
	componentWillUnmount(){
	}
    requestContent(){
        var index = this.props.index;
        BleAction.RequestPeripheralContent(index);
    }
	render(){
		return(
			<TouchableHighlight style={styles.veiwBlock} onPress={()=>this.requestContent()}>
                <View style={styles.rectBlock}>
                    <View style={[styles.rect,this.props.rectColor]}></View>
                    <Text style={styles.text}>{this.props.name}</Text>
                </View>
            </TouchableHighlight>
		)
	}
}