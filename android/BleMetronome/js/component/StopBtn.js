'use strict';
import React, { Component } from 'react';
import {View, Text , TouchableHighlight} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import BleStateStore from '../store/BleStateStore';
import * as BleAction from '../action/BleAction';

const styles=EStyleSheet.create({
    text:{
        color:'#a29494',
        fontSize:13,
    },
    btn:{
        flex:1,
        borderWidth:1,
        borderColor:'#a29494',
        alignItems:'center',
        justifyContent:'center',
    }
});

export default class StopBtn extends Component {
	constructor(props){
		super(props);
        this.fireStop=this.fireStop.bind(this);
        this.state={
            status:'Status',
        }
	}
	componentWillMount(){
	}
	componentWillUnmount(){
	}
    fireStop(){
        BleAction.fireStop();
    }
	render(){
		return(
			<TouchableHighlight style={[styles.btn,]} onPress={()=>{this.fireStop()}}>
                <View style={{flex:1,justifyContent:'center',}}>
                    <Text style={styles.text}>Stop all deveces</Text>
                </View>
            </TouchableHighlight>
		)
	}
}