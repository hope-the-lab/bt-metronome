import React, { Component } from 'react';
import { View, Text } from 'react-native';
import FireUpBtn from './component/fireUpBtn';
import IntroInfo from './component/introinfo'
import EStyleSheet from 'react-native-extended-stylesheet';
import { Actions } from 'react-native-router-flux';


const styles = EStyleSheet.create({
		infoFrame: {
			backgroundColor: '#d3d3d3',
			height: '80%'
		}
		, btnFrame: {
			backgroundColor:'#565656',
			height: '20%'
		}
	});

export default class PageOne extends Component {
  render() {
    return (
		<View>
			<View style={styles.infoFrame}>
				<IntroInfo />
			</View>
			<View style={styles.btnFrame}>
				<FireUpBtn title="Scan" />
			</View>
		</View>
    )
  }
}