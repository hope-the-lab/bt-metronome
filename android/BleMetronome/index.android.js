/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import PageOne from './js/onePage';
import PageTwo from './js/twoPage';
import TestPage from './js/TestPage';
import ConnectPage from './js/ConnectPage';
import { Router, Scene ,Actions,ActionConst } from 'react-native-router-flux';
import EStyleSheet from 'react-native-extended-stylesheet';
import BleStateStore from './js/store/BleStateStore';
import * as BleAction from './js/action/BleAction';

import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';

export default class BleMetronome extends Component {
  constructor(){
	  super();
	  BleAction.InitBle();
  }
  componentWillUnmount(){
	  BleAction.ResetBle();
  }

  render() {
    return (
	  <Router>
		<Scene key="root" hideNavBar="false">
          <Scene key="pageOne" component={PageOne} title="hi" initial={true} />
          <Scene key="pageTwo" component={PageTwo} title="wow" />
		  <Scene key="TestPage" component={TestPage} title="Test" />
          <Scene key="ConnectPage" component={ConnectPage} title="connect" type={ActionConst.REPLACE}/>
        </Scene>
	  </Router>
    );
  }
}
EStyleSheet.build();
AppRegistry.registerComponent('BleMetronome', () => BleMetronome);
