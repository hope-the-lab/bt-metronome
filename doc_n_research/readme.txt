windows : shift+右鍵 可以快速在資料夾上打開CMD !

-------------------------------------

[git 說明]

	https://git-scm.com/
	[建置SSH KEY]
	$> ssh-keygen -t rsa -f ~/.ssh/id_rsa
	(P.S. passphrase可以不輸入省去以後打密碼的麻煩)
	
	https://bitbucket.org/ > bitbucket settings > SSH KEY > ADD
	貼上 C:/users/user/.ssh/ 目錄下 Public key 的內容
	
	[第一次複製專案]
	$> git clone git@bitbucket.org:hope-the-lab/bt-metronome.git
	
	[基本操作]
	$> git pull origin master  /* 與git server 同步 */
	$> git add .               /* 收錄新增加的檔案 */
	$> git commit -am"(不留敘述git會被lock住喔!)"  /* 建立儲存點 */
	$> git push origin master /* 將儲存點內容推上 git server */
	$> git status /*顯示當前狀態*/
	
	[commit合併操作]
	
	origin/master 0-0-0-0-0-a-a                        
	(local)master 0-0-0-0-0-b
	
	遇到這種狀況的後git可能會要求merge
	此時會出現<master|MERGING>的訊息,不用慌張，打開git status照著做就行了
	期間可能會要求commit 、add，這就比較無所謂，不用怕蓋到檔案
	雖然說git會"自行做比對"檢查不一樣的commit,但是他不會自己擅自幫你執行合併跟commit，
	只會一直提示你怎麼樣可以讓你commit回origin/master的狀態哈哈。
	所以這部份的操作沒有一定的流程，跟著git status做就對了。
	
	>>特殊狀況 --出現文檔編輯輯器
	這個時候代表git判斷出你跟你的協作者動到同一段code了，
	1. ctrl+z 離開編輯器
	2. git status 找到有異動的檔案
	3. 打開那支檔案找到以下段落
	
	======================>HEAD
	
	你修改的版本
	
	-------------------------
	
	協作者修改的版本
	
	====================>zxcasdsvxcv
	
	4. 調整好檔案後重新commit即可.
	
	[進階]
	加註在.gitignore裡的檔案會被git忽略，可以防止不必要的檔案被更新上去。
	
--------------------------------------	

[bleno-master/noble-master 使用文件]

執行需求:

	for Windows:
	Virtual studio 2015 [建議]
	Virtual C++ 可轉發套件

	for all:
	nodejs (實測 Win 10 x64 node 7.1.0 不會過)
	phython 2.7 (phython3的語言架構不一樣，請勿越級打怪)

安裝方式:
	
	於專案目錄下輸入 npm install 即可一次安裝所有moudle
	
	for Windows:
	使用zadig 置換 藍芽裝置驅動程式

reference : https://github.com/sandeepmistry/noble